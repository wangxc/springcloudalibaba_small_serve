package com.itmuch.contentcenter.feignclient;


import com.itmuch.contentcenter.domain.dto.user.UserDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient("microservice-provider-user")
public interface TestUserFeignClient {

    @GetMapping("/get")
    public UserDTO get0(@SpringQueryMap UserDTO userDTO);
}