package com.itmuch.contentcenter.service.content;

import com.itmuch.contentcenter.dao.content.ShareMapper;
import com.itmuch.contentcenter.domain.dto.content.ShareDTO;
import com.itmuch.contentcenter.domain.dto.user.UserDTO;
import com.itmuch.contentcenter.domain.entity.content.Share;
import com.itmuch.contentcenter.feignclient.UserCenterFeignClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ShareService {

    @Autowired
    private ShareMapper shareMapper;
//    @Autowired
//    private RestTemplate restTemplate;
    
    @Autowired
    private UserCenterFeignClient userCenterFeignClient;

//    @Autowired
//    private DiscoveryClient discoveryClient;

    public ShareDTO findById(Integer id){
        //获取分享详情
        Share share=shareMapper.selectByPrimaryKey(id);
        //发布人id
        Integer userId=share.getUserId();

        //1.代码不可读
        //2.复杂的url难以维护,
        //3.难以响应需求的变化,变化很没有幸福感
        //4.编程体验不统一
//        UserDTO userDTO=restTemplate.getForObject(
//                "http://user-center/users/{userId}",
//                UserDTO.class,userId
//        );

        UserDTO userDTO = userCenterFeignClient.findById(id);


        ShareDTO shareDTO = new ShareDTO();
        BeanUtils.copyProperties(share,shareDTO);
        shareDTO.setWxNickname(userDTO.getWxNickname());
        return shareDTO;
    }

    public static void main(String[] args) {
        RestTemplate restTemplate=new RestTemplate();
        ResponseEntity<String> forEntity=restTemplate.getForEntity(
                "http://localhost:8080/users/1",
                String.class,2
        );
        System.out.println(forEntity.getBody());
    }
}
