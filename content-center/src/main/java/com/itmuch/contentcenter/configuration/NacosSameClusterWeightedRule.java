package com.itmuch.contentcenter.configuration;

import com.alibaba.nacos.api.naming.NamingService;
import com.alibaba.nacos.api.naming.pojo.Instance;
import com.alibaba.nacos.client.naming.core.Balancer;
import com.netflix.client.config.IClientConfig;
import com.netflix.loadbalancer.AbstractLoadBalancerRule;
import com.netflix.loadbalancer.BaseLoadBalancer;
import com.netflix.loadbalancer.Server;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.alibaba.nacos.NacosDiscoveryProperties;
import org.springframework.cloud.alibaba.nacos.ribbon.NacosServer;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
public class NacosSameClusterWeightedRule extends AbstractLoadBalancerRule {

    @Autowired
    private NacosDiscoveryProperties nacosDiscoveryProperties;

    @Override
    public void initWithNiwsConfig(IClientConfig iClientConfig) {

    }

    @Override
    public Server choose(Object o) {
        //1.找到指定服务的所有实例 A
        //2.过滤出相同集群下的所有实例 B
        //3.如果B是空,就用A
        //4.基于权重的负载均衡算法,返回1个实例

        try {
            //获取到配置中的集群名称 SH
            String clusterName = nacosDiscoveryProperties.getClusterName();

            BaseLoadBalancer loadBalancer = (BaseLoadBalancer) this.getLoadBalancer();

            //想要请求的微服务的名称
            String name = loadBalancer.getName();
            //拿到服务发现的相关API
            NamingService namingService = nacosDiscoveryProperties.namingServiceInstance();

            //1.找到指定服务的所有实例
            List<Instance> instances = namingService.selectInstances(name, true);

            //2.过滤出相同集群下的所有实例
            List<Instance> sameClusterInstances = instances.stream()
                    .filter(instance -> Objects.equals(instance.getClusterName(), clusterName))
                    .collect(Collectors.toList());

            // 3.如果上述2为空，就用1
            List<Instance> instancesToBeChosen = new ArrayList<>();
            if (CollectionUtils.isEmpty(sameClusterInstances)) {
                instancesToBeChosen = instances;
                log.warn("发生了跨集群的调用，name = {}, clusterName = {}, instances = {}", name, clusterName, instances);
            } else {
                instancesToBeChosen = sameClusterInstances;
            }

            //4.基于权重的负载均衡算法，返回一个实例
            Instance instance = ExtendsBalancer.getHostByRandomWeightExtends(instancesToBeChosen);
            log.info("选择的实例是  instance = {}", instance);
            return new NacosServer(instance);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}

class ExtendsBalancer extends Balancer {
    public static Instance getHostByRandomWeightExtends(List<Instance> hosts) {
        return getHostByRandomWeight(hosts);
    }
}