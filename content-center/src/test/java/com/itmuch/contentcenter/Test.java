package com.itmuch.contentcenter;

import sun.applet.Main;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class Test {
    //正则表达式
    //输出格式 List<Airport>

    //参考正则代码


    private final String IATAREG = "\\s*IATA:\\s*[^\\n]+";
    private final String ICAOREG = "\\s*ICAO:\\s*[^\\n]+";

    public static void main(String[] args) {
        Test test = new Test();
        test.main();
    }

    public void main() {
        String sourcetxt = "E:\\test\\airport.txt";
        String str = "";
        try {
            // 在给定从中读取数据的文件名的情况下创建一个新 FileReader
            FileReader fr = new FileReader(sourcetxt);

            // 创建一个使用默认大小输入缓冲区的缓冲字符输入流
            BufferedReader br = new BufferedReader(fr);
            String strTmp = "";
            while (null != (strTmp = br.readLine())) {
//                System.out.println(strTmp);
                str+=strTmp+"|";
            }
        }catch (Exception e){
            e.printStackTrace();
        }

//        System.out.println(getMatchField(Pattern.compile(IATAREG), strTmp));
        System.out.println(str);
        String IATA = getFieldValue(getMatchField(Pattern.compile(IATAREG), str), ":");
    }



    protected String getMatchField(Pattern pattern, String source) {
        Matcher matcher = pattern.matcher(source);
        if(matcher.find()) {
            return getMatchString(matcher, source);
        }
        return null;
    }

    protected String getMatchString(Matcher matcher, String source) {
        return source.substring(matcher.start(), matcher.end());
    }


    protected String getFieldValue(String str,String regx) {

        return null;
    }
}


class Airport{
    private String Name;//机场名(必填)
    private String City;//城市(必填)
    private String State;//省(必填)
    private String Country;//国家(必填)
    private String IATA;//IATA(必填)
    private String ICAO;//ICAO(可以空)
    private String FAA;//FAA(可以空)
    private String LCL;//LCL(可以空)
    private String Latitude1;//纬度1(必填)
    private String Latitude2;//纬度2(必填)
    private String Longitude1;//经度1(必填)
    private String Longitude2;//经度2(必填)
    private String Elevation1;//海拔1(必填)去掉 feet MSL
    private String Elevation2;//海拔2(必填)去掉  m MSL
    private String Magnetic;//磁变(必填)去掉 E及后面，只留数字
    private String TimeZone1;//时区1(必填)去掉 UTC和(Standard Time)，只留数字
    private String TimeZone2;//时区2(必填)去掉 UTC和(Standard Time)，只留数字


}